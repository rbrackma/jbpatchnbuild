%define dist el6
%define jbver 6.2.0
Summary: Red Hat JBoss Enterprise Application Platform
Name: jboss-eap-%jbver
Version: 0
Release: 0.%dist
License: GPL
Group: Applications/System
URL: http://www.jboss.org
Source: %{name}.tar.gz
#Requires: java-1.6.0-openjdk-devel, bash
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
BuildArch: noarch
AutoReqProv: no
Prefix: /opt

%description
Red Hat JBoss Enterprise Application Platform Custom RPM

%prep
%setup -n %{name}

%build
#Nothing. Program is already built...

%install
%{__rm} -rf $RPM_BUILD_ROOT
%{__mkdir} -p $RPM_BUILD_ROOT/opt/jboss-eap/%jbver
%{__mkdir} -p $RPM_BUILD_ROOT/var/log/jboss-eap/%jbver
%{__cp} -R ./* $RPM_BUILD_ROOT/opt/jboss-eap/%jbver
%{__cp} -R ./.installation $RPM_BUILD_ROOT/opt/jboss-eap/%jbver
mkdir -p $RPM_BUILD_ROOT/opt/jboss-eap/%jbver/.installation
touch $RPM_BUILD_ROOT/opt/jboss-eap/%jbver/.installation/installation.txt

%clean
%{__rm} -rf $RPM_BUILD_ROOT

%files
%defattr(-,jboss,jboss,-)
%attr(0755,jboss,jboss) %dir /opt/jboss-eap/%jbver
/opt/jboss-eap/%jbver/*
%attr(0755,jboss,jboss) %dir /opt/jboss-eap/%jbver/.installation
/opt/jboss-eap/%jbver/.installation/*
%doc
#%attr(0755,jboss,jboss) %dir /var/log/jboss-eap/%jbver

%pre
  # Add user/group here if needed
  /usr/sbin/groupadd jboss > /dev/null 2>&1 || :
  /usr/sbin/useradd -M -r -d /opt/jboss-eap/%jbver -s /bin/bash -c "JBoss Application Server" -g jboss jboss > /dev/null 2>&1 || :
  mkdir -p /var/log/jboss-eap/%jbver
  chown jboss:jboss /var/log/jboss-eap/%jbver > /dev/null 2>&1 || :


%post

%preun

%postun

%changelog
* Thu Jan 16 2014 Louis Coilliot
- first build
