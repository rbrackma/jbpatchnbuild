#!/bin/bash

VER=6.2.0
#VER=6.3.0
FLAVOR=jboss-eap
WAITTIME=5  # seconds to wait for jboss after a restart
RETRYWAIT=5 # times we wait and test that jboss is up

JB=${FLAVOR}-${VER}

INSTBASE=/opt

BLDDIR=~/rpmjbbuild
SRCDIR=$BLDDIR/SOURCES
SPCDIR=$BLDDIR/SPECS

ORIHOME=$HOME
CURRENTUSER=$(id -un)

error() { echo "$1"; exit 1; }

# Because 7za can truncate the root archive path
rpm -q p7zip &>/dev/null || error \
  'Please install p7zip first (needs EPEL).'

rpm -q java-1.7.0-openjdk &>/dev/null || error \
  'Please install java-1.7.0-openjdk first.'

test -f src/${JB}.zip    || error \
  "Please provide src/${JB}.zip from http://www.jboss.org/products/eap/download"
test -f specs/${JB}.spec || error \
  "Please provide specs/${JB}.spec"

function waitrestart {
  echo "Wait $WAITTIME sec for restart"
  sleep $WAITTIME
  count=$RETRYWAIT
  until [ $count = 0 ];do
    echo "Test that the CLI is up"
    $INSTBASE/$FLAVOR/$VER/bin/jboss-cli.sh --connect version 2>&1 | grep -q 'JBoss AS product'
    if [ $? -ne 0 ];then
      echo "Not yet. Wait $WAITTIME sec more"
      sleep $WAITTIME
    else
      return
    fi
    let "count--"
  done
  echo 'Still not. Fail and exit.'
  exit 1
}

function purgebuild {
  rm -rf $BLDDIR/{BUILD,RPMS,SOURCES,SPECS,SRPMS}
  mkdir -p $BLDDIR/{BUILD,RPMS,SOURCES,SPECS,SRPMS}
}

function unzipsrc {
  echo 'Unzip src'
  rm -rf $BLDDIR/SOURCES
  mkdir -p $BLDDIR/SOURCES
  7za x src/${JB}.zip -o${SRCDIR} > /dev/null

  # jboss-eap-x.y.z.zip from jboss.org unzips to jboss-eap-x.y
  # Well done, guys ! ><
  jbmajver=$(echo $JB | sed 's/\.[0-9]$//g') # truncate minor ver
  mv $SRCDIR/$jbmajver $SRCDIR/$JB # Rename to jboss-eap-x.y.z, expected by the
                                   # spec file
  7za a -tzip $SRCDIR/$JB.zip $SRCDIR/$JB > /dev/null
  rm -rf $SRCDIR/$JB
}

function setspec {
  rm -rf $BLDDIR/SPECS
  mkdir -p $BLDDIR/SPECS
  echo 'Set the spec file'
  cp specs/${JB}.spec $SPCDIR
  sed -i "s/[\t ]*Source:.*/Source:${JB}.zip/g" $SPCDIR/${JB}.spec
}

function buildrpm {
  echo 'Build the rpm'
  rm -rf $BLDDIR/{RPMS,SRPMS}
  mkdir -p $BLDDIR/{RPMS,SRPMS}
  rpmbuild --define "_topdir $BLDDIR" -ba $SPCDIR/${JB}.spec &>/dev/null
  rm -rf $BLDDIR/BUILD
  echo 'Result :'
  find $BLDDIR/RPMS $BLDDIR/SRPMS -name '*.rpm'
}

function installrpm {
  echo 'Install the rpm'
  sudo rpm -ivh --prefix "$INSTBASE" "$BLDDIR/RPMS/noarch/${JB}-0-0.el6.noarch.rpm"
}

function startjb {
  echo 'Start Jboss'
  sudo usermod -d "$INSTBASE/$FLAVOR/$VER" jboss
  sudo su - jboss -c "nohup $INSTBASE/$FLAVOR/$VER/bin/standalone.sh > /tmp/jbstart.log &"
  waitrestart
}

function patchjb {
  oriifs=$IFS
  IFS=';'
  while read line; do
    set -- $line
    pfile=$1
    pdesc=$(echo $2 | sed 's/^[ \t]*//')
    echo "Apply patch : $pfile"
    echo "Desc : $pdesc"
    $INSTBASE/$FLAVOR/$VER/bin/jboss-cli.sh --connect --command="patch apply src/patchs-$VER/$1 --override-all"
    echo 'Restart Jboss'
    $INSTBASE/$FLAVOR/$VER/bin/jboss-cli.sh --connect command=":shutdown(restart=true)"
    waitrestart
    echo 'Test the Jboss version'
    $INSTBASE/$FLAVOR/$VER/bin/jboss-cli.sh --connect version | grep 'JBoss AS product'
  done < patchs-${VER}.lst
  IFS=$oriifs
}

function stopjb {
  echo 'Stop Jboss'
  $INSTBASE/$FLAVOR/$VER/bin/jboss-cli.sh --connect command=:shutdown
}

function cleansrc {
  sudo rm -rf $INSTBASE/$FLAVOR/$VER/standalone/log
  sudo rm -rf $INSTBASE/$FLAVOR/$VER/standalone/data
  sudo rm -rf $INSTBASE/$FLAVOR/$VER/standalone/tmp/{vfs,work}
}

function zippatched {
  echo 'Zip the patched version'
  sudo mv $INSTBASE/$FLAVOR/$VER $INSTBASE/$FLAVOR/$JB
  sudo 7za a -l -tzip $INSTBASE/$JB.zip $INSTBASE/$FLAVOR/$JB > /dev/null
  sudo mv $INSTBASE/$FLAVOR/$JB $INSTBASE/$FLAVOR/$VER
  sudo mv $INSTBASE/$JB.zip $SRCDIR
  sudo chown $CURRENTUSER $SRCDIR/$JB.zip
}

function patchspec {
    rpmver=$(grep -i cumulative patchs-${VER}.lst | wc -l)
    rpmrel=$(grep -vi cumulative patchs-${VER}.lst | wc -l)
    sed -i "s/Version:.*/Version: $rpmver/g" $SPCDIR/${JB}.spec
    sed -i "s/Release:.*/Release: $rpmrel.%dist/g" $SPCDIR/${JB}.spec
    sed -i "/%changelog/q" $SPCDIR/${JB}.spec
    datef=$(LC_TIME=en_US date +"%a %b %e %Y")
    oriifs=$IFS
    IFS=';'
    tac patchs-${VER}.lst | while read line; do
      set -- $line
      pfile=$1
      pdesc=$(echo $2 | sed 's/^[ \t]*//')
      echo "* $datef Version $rpmver.$rpmrel" >> $SPCDIR/${JB}.spec
      echo "- Patch $1" >> $SPCDIR/${JB}.spec
      echo "- $2" >> $SPCDIR/${JB}.spec
      echo $pdesc | grep -qi cumulative \
          && { let "rpmver--"; rel=0; }  \
          || { let "rel=rpmrel"; let "rpmrel--"; }
    done
    IFS=$oriifs
    echo "* $datef Version 0.0" >> $SPCDIR/${JB}.spec
    echo '- Base version' >> $SPCDIR/${JB}.spec
}

function uninstallrpm {
  echo 'Uninstall the rpm'
  sudo rpm -e ${JB}-0-0.el6.noarch
  sudo rm -rf $INSTBASE/$FLAVOR
}

purgebuild
unzipsrc
setspec
buildrpm
installrpm
startjb
patchjb
stopjb
cleansrc
zippatched
uninstallrpm
patchspec
buildrpm

